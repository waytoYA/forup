import React, { createContext } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './less/main.css'
import Todolist from './context/Todolist'

export const TodoContext = createContext(null)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <TodoContext.Provider value={{
        todolist: new Todolist(),
    }}>
        <App />
    </TodoContext.Provider>
);