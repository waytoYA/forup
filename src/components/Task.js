import React, {  useState,  useContext } from 'react';
import {TodoContext} from '../index'


const Task = ({id, close}) => {


  const {todolist} = useContext(TodoContext)

  const [title, setTitle] = useState(id == 0 ? '' : todolist.tasks[id - 1].title)
  const [description, setDescription] = useState(id == 0 ? '' : todolist.tasks[id - 1].description)
  const [date, setDate] = useState(id == 0 ? '' : todolist.tasks[id - 1].date)

  /**
   * @description - Если id = 0 - выполняем добавление задачи, иначе изменяем существующую
   */
  const save = () => {

    if (id == 0){
      const newId = todolist.tasks.length + 1
      todolist.setTasks([...todolist.tasks, {'id': newId, 'title': title || 'Без названия', 'description': description, 'date': date, 'active': false}])
    } else {
      todolist.setTasks(todolist.tasks.map(task => task.id == id ? {...task, 'id': id, 'title': title, 'description': description, 'date': date} : task))
    }

    close(false)

  }

  return (
      <div className='task'>

        <input 
          className='task-title'
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />

        <textarea 
          className='task-description' 
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />

        <div className='task-date'>До: 
          <input 
            type='date'
            value={date}
            onChange={(e) => setDate(e.target.value)}
          />
        </div>

        <input 
          id='task-file'
          className='task-fileReal'
          type='file'
        />
        <label 
          htmlFor='task-file'
          className='task-fileFake'
        >Добавить файл</label>

        <button 
          className='task-button button' 
          onClick={() => save()}
        >
          Сохранить
        </button>

      </div>
  )
}

export default Task;