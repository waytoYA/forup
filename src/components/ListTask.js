import React, {  useState,  useEffect, useContext } from 'react';


const ListTask = ({id, title, date, active, completedTask, openTask, deleteTask}) => {

    return (
    <div 
        className='todo_block_list-task' 
        key={id}
    >

        <input 
            id={`check-${id}`}
            className='todo_block_list-task-realCheckbox'
            type='checkbox'
            checked={active}
            onChange={(e) => completedTask(e.target.checked, id)}
        />
        <label className='todo_block_list-task-fakeCheckbox' htmlFor={`check-${id}`}></label>

        <div 
            className={`todo_block_list-task-info ${active ? 'list-task-active' : ''}`}
            onClick={() => openTask(id)}
        >

        <h1>{title}</h1>
        <h3>{date}</h3>

        </div>

        <div className='todo_block_list-task-delete' onClick={(e) => deleteTask(id)}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20" height="20"><g id="_01_align_center" data-name="01 align center"><path d="M22,4H17V2a2,2,0,0,0-2-2H9A2,2,0,0,0,7,2V4H2V6H4V21a3,3,0,0,0,3,3H17a3,3,0,0,0,3-3V6h2ZM9,2h6V4H9Zm9,19a1,1,0,0,1-1,1H7a1,1,0,0,1-1-1V6H18Z"/><rect x="9" y="10" width="2" height="8"/><rect x="13" y="10" width="2" height="8"/></g></svg>
        </div>

    </div>
  )
}

export default ListTask;