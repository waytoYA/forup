class Todolist{
    constructor(){
        this._tasks = [
            {
            id: 1,
            title: 'Задача №1',
            description: 'Какое-то описание...',
            date: '2023-11-20',
            active: false,
            },
            {
            id: 2,
            title: 'Задача №2',
            description: 'Какое-то описание...',
            date: '2022-11-18',
            active: false,
            },
        ]
    }

    get tasks(){
        return this._tasks
    }

    setTasks(tasks) {
        this._tasks = tasks
    }
}

export default Todolist;