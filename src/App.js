import React, {  useState,  useEffect, useContext } from 'react';
import ListTask from './components/ListTask';
import Task from './components/Task';
import {TodoContext} from './index'

const App = () => {

    const dayjs = require('dayjs')

    const {todolist} = useContext(TodoContext)

    const [activatedTask, setActivatedTask] = useState(false)
    const [idTask, setIdTask] = useState(0)

    /**  Производит обновление компонента   */
    const [updateValue, startUpdate] = useState(0)

    /**
     * 
     * @param {number} id - id задачи, которую нужно открыть 
     */
    const openTask = (id) => {
      setIdTask(id)
      setActivatedTask(true)
    }

    /**
     * @description Создание задачи
     */
    const createTask = () => {
      setIdTask(0)
      setActivatedTask(true)
    }

    /**
     * 
     * @param {boolean} value - значение выполнения у задачи
     * @param {number} id - id задачи
     */
    const completedTask = (value, id) => {
      todolist.setTasks(todolist.tasks.map(task => task.id === id ? {...task, 'active': value} : task))
      startUpdate(updateValue + 1)
    }

    /**
     * 
     * @param {number} id - id задачи, которую надо удалить
     */
    const deleteTask = (id) => {
      todolist.setTasks(todolist.tasks.filter(task => task.id != id))

      let i = 1
      todolist.setTasks(todolist.tasks.map(task => {
        task.id = i
        i += 1
        return {...task}
      }))

      startUpdate(updateValue + 1)
    }

    useEffect(()=>{

      todolist.setTasks(todolist.tasks.map(task => {
        let now = dayjs().format()
        let dateTask = dayjs(task.date)

        return {...task, 'active': dateTask.diff(now) < 0 || task.active}
      }))

      startUpdate(updateValue + 1)

    }, [activatedTask])

    return (
        <div className='todo'>
          <div className='todo_block'>

            <div className='todo_block-title'>
                {activatedTask ? 'Ваша задача' : 'Список задач'}
            </div>

            {activatedTask

            ?

            <Task
              id={idTask}
              close={setActivatedTask}
            />

            :

            <div className='todo_block_list'>

                  {todolist.tasks.map(task => 
                      <ListTask 
                        id={task.id}
                        title={task.title}
                        date={task.date}
                        active={task.active}
                        completedTask={completedTask}
                        openTask={openTask}
                        deleteTask={deleteTask}

                        key={task.id}
                      />
                  )}

                  <button className='todo_block_list-button button' onClick={() => createTask()}>
                    Создать
                  </button>
                  
            </div>
            
            }   
          </div>

        </div>
    )
}

export default App;